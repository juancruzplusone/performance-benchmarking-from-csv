from serial_comm_lib.serial_comm_lib import SerialCommLib
import pandas as pd
import csv
import time
from datetime import datetime


class PerformanceBenchmarks:
    """ Class to facilitate performance benchamrks for research into PLC2SBC transition.

    This class uses "record" methods to create CSV from serial commnication data sent over by SBC acting as a PLC.
    The CSVs are analyzed when the benchmarkResults method is called. The resulting output is printed to terminal and
    outputted as a CSV that can be reviewed and printed with the same method at a later point.

    the private "analyze" methods are used internally to fill in benchmark result fields. 
    """

    def __init__(self):
        pass

    def recordRealTimeData(self, duration, pathToOutput):
        """ Creates CSV file of recieved serial data.

        Args:
            duration (int): Length of time to record in seconds

        Out:
            CSV file to specified directory. Defaults to root if no input
        """
        self.serial_lib.listen()  # Start listening for data

        start_time = time.time()  # Record start time

        scan_start_time = None

        with open(pathToOutput, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Timestamp', 'Data', 'ScanTime'])  # Write the header of CSV

            while time.time() - start_time < duration:
                incoming_data = self.serial_lib.getIncomingData()

                for data in incoming_data:
                    timestamp, message = data.split(',', 1)
                    if "START" in message:
                        scan_start_time = timestamp
                    elif "END" in message and scan_start_time is not None:
                        scan_time = float(timestamp) - float(scan_start_time)
                        writer.writerow([datetime.now(), data, scan_time])  # Write timestamp, data, and scan time to CSV
                        scan_start_time = None  # Reset for the next loop

        self.serial_lib.stopListening()  # Stop listening after duration

    def __analyzeRealTimeCSV(self):
        """Reads scan times from CSV file and calculates statistical measures.

        Args:
            pathToOutput (str): The path to the CSV file.

        Returns:
            dict: A dictionary containing the average, min, max, and standard deviation of scan times.
        """
        # Read CSV file
        df = pd.read_csv()

        # Ensure 'ScanTime' column is numeric
        df['ScanTime'] = pd.to_numeric(df['ScanTime'])

        # Calculate statistical measures
        avg_scan_time = df['ScanTime'].mean()
        min_scan_time = df['ScanTime'].min()
        max_scan_time = df['ScanTime'].max()
        std_dev_scan_time = df['ScanTime'].std()

        return {
            'average': avg_scan_time,
            'min': min_scan_time,
            'max': max_scan_time,
            'std_dev': std_dev_scan_time,
        }


    def recordRealiabilityData(self):
        pass

    def recordInOutData(self):
        pass

    def recordNetworkPerformanceData(self):
        pass

    def __analyzeRealiabilityCSV(self):
        pass

    def __analyzeInOutCSV(self):
        pass

    def __analyzeNetworkPerformanceCSV(self):
        pass

    def benchmarkResults(self, pathToRealTimeCSV):
        """Analyzes and prints out the real-time performance results."""
        results = self.__analyzeRealTimeCSV(pathToRealTimeCSV)
        print(f"Average Scan Time: {results['average']}")
        print(f"Minimum Scan Time: {results['min']}")
        print(f"Maximum Scan Time: {results['max']}")
        print(f"Standard Deviation of Scan Time: {results['std_dev']}")

"""
if __name__ == '__main__':

    rpiBenchmarks = PerformanceBenchmarks()

    rpiBenchmarks.importRealTimeMeasure("example.csv")
    # ...

    rpiBenchmarks.benchmarkResults()


"""

